import { Schema, model, Document, Model } from 'mongoose'

export interface HostelRoomDocument extends Document {
    isBooked: boolean
    roomNumber: string
    userId: string
    hostelId: string
}


const hostelRoomSchema = new Schema({
    isBooked: { type: Boolean, default: false },
    roomNumber: String,
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        autopopulate: true
    },
    hostelId: {
        type: Schema.Types.ObjectId,
        ref: 'HostelRoom',
        autopopulate: true
    }
}, {
    timestamps: true
})

hostelRoomSchema.set('toJSON', {
    transform: (doc, { __v, password, ...rest }, options) => rest
})

export const HostelRoom = model<HostelRoomDocument>('HostelRoom', hostelRoomSchema)
