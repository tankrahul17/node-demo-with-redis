import { Schema, model, Document, Model } from 'mongoose'

export interface HostelDocument extends Document {
    name: string
}


const hostelSchema = new Schema({
    name: String
}, {
    timestamps: true
})

hostelSchema.set('toJSON', {
    transform: (doc, { __v, password, ...rest }, options) => rest
})

export const Hostel = model<HostelDocument>('Hostel', hostelSchema)
