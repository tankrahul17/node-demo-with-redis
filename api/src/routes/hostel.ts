import { Router } from 'express'
import { BadRequest } from '../errors'
import { sendMail } from '../mail'
import { auth, catchAsync } from '../middleware'
import { Hostel, HostelRoom, User } from '../models'
import { validate, hostelSchema, hostelRoomSchema, hostelRoomBookSchema, availableRoomSchema } from '../validation'

const router = Router()

router.get('/hostels', auth, catchAsync(async (req, res) => {
  const hostels = await Hostel.find()
  const json = {
    status: 200,
    data: hostels,
    message: "Hostels found successfully!!"
  }
  res.json(json)
}))

router.post('/hostelRooms', auth, catchAsync(async (req, res) => {
  await validate(availableRoomSchema, req.body)

  const { hostelId } = req.body

  const hostelRoom = await HostelRoom.find({ hostelId })
  const json = {
    status: 200,
    data: hostelRoom,
    message: "Hostel rooms found successfully!!"
  }
  res.json(json)
}))

router.post('/hostel', auth, catchAsync(async (req, res) => {
  await validate(hostelSchema, req.body)

  const { name } = req.body

  const found = await Hostel.exists({ name })

  if (found) {
    throw new BadRequest('Hostel name should be uniqe!!')
  }

  const hostel = await Hostel.create({
    name
  })

  const json = {
    status: 200,
    data: hostel,
    message: "Hostel Created successfull!!"
  }
  res.json(json)
}))

router.post('/hostelRoom', auth, catchAsync(async (req, res) => {
  await validate(hostelRoomSchema, req.body)

  const { hostelId, roomNumber } = req.body

  const found = await HostelRoom.findOne({ hostelId: hostelId, roomNumber: roomNumber })

  if (found) {
    throw new BadRequest('Room number should be uniqe!!')
  }

  const room = await HostelRoom.create({
    hostelId, roomNumber
  })

  const json = {
    status: 200,
    data: room,
    message: "Room added successfull!!"
  }
  res.json(json)
}))

router.patch('/roomBook', auth, catchAsync(async (req, res) => {
  await validate(hostelRoomBookSchema, req.body)

  const { roomId, userId } = req.body

  const found = await HostelRoom.findOne({ _id: roomId, isBooked: true })

  if (found) {
    throw new BadRequest('Room already booked!!')
  }
  await HostelRoom.updateOne({ _id: roomId }, { userId, isBooked: true })

  const user = await User.findById(userId)
  if (user) {
    await sendMail({
      to: user.email,
      subject: 'Verify your email address',
      text: "Enjoy, your room booking confirm!!"
    })
  }
  const json = {
    status: 200,
    message: "Room booked successfull!!"
  }
  res.json(json)
}))

router.post('/availableRoom', auth, catchAsync(async (req, res) => {
  await validate(availableRoomSchema, req.body)

  const { hostelId } = req.body

  const hostelRoom = await HostelRoom.find({ hostelId, isBooked: false })

  const json = {
    status: 200,
    data: hostelRoom,
    message: "Room find successfull!!"
  }
  res.json(json)
}))

export { router as hostel }
