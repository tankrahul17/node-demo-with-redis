import { Router } from 'express'
import { catchAsync, guest, auth } from '../middleware'
import { validate, loginSchema } from '../validation'
import { User } from '../models'
import { BadRequest } from '../errors'
import { logIn, logOut } from '../auth'

const router = Router()

router.post('/login', guest, catchAsync(async (req, res) => {
  await validate(loginSchema, req.body)

  const { email, password } = req.body

  const user = await User.findOne({ email })

  if (!user || !(await user.matchesPassword(password))) {
    throw new BadRequest('Incorrect email or password')
  }

  logIn(req, user.id)
  const json = {
    status: 200,
    message: "Login successfull!!"
  }
  res.json(json)
}))

router.post('/logout', auth, catchAsync(async (req, res) => {
  await logOut(req, res)
  const json = {
    status: 200,
    message: "Logout successfull!!"
  }
  res.json(json)
}))

router.get('/users', auth, catchAsync(async (req, res) => {
  const users = await User.find()
  const json = {
    status: 200,
    data: users,
    message: "Users found successfully!!"
  }
  res.json(json)
}))

export { router as login }
