import { Joi } from './joi'
const hostelId = Joi.objectId().required()
const userId = Joi.objectId().required()
const roomId = Joi.objectId().required()

export const hostelSchema = Joi.object({
    name: Joi.string().trim().required()
})

export const hostelRoomSchema = Joi.object({
    hostelId,
    roomNumber: Joi.string().trim().required()
})

export const hostelRoomBookSchema = Joi.object({
    roomId,
    userId 
})

export const availableRoomSchema = Joi.object({
    hostelId 
})
